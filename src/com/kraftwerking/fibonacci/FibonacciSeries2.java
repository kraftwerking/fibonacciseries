package com.kraftwerking.fibonacci;

public class FibonacciSeries2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// print fibonacci series up to 7

		for (int i = 0; i <= 7; i++) {
			System.out.print(fibonacci(i) + " ");

		}
	}

	//recursive solution
	public static int fibonacci(int i) {
		if (i == 0) {
			return 0;
		} else if (i == 1) {
			return 1;
		} else {
			return fibonacci(i - 1) + fibonacci(i - 2);
		}

	}
}
