package com.kraftwerking.fibonacci;

public class FibonacciSeries {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// print fibonacci series up to 7
		System.out.print(fibonacci(7));
	}
	public static String fibonacci(int i){
		
		if(i==0){
			return "0";
		} else if (i==1){
			return "0 1";
		} else {
			int a = 0;
			int b = 1;
			int nextNumber;
			StringBuilder sb = new StringBuilder("0 1 ");
			for(int x=2;x<=i;x++){
				nextNumber = a + b;
				a = b;
				b = nextNumber;
				sb.append(nextNumber + " ");
			}
			return sb.toString();
		}
		
	}
}
