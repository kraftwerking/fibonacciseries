package com.kraftwerking.fibonacci;

public class FibonacciSeries3 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// print fibonacci series up to 7
		//0-1
		System.out.print("0 1 ");
		fibonacci(7); //starting from 2

	}
	
	public static void fibonacci(int i){
		int a = 0;
		int b = 1;
		int nextNumber;
		for (int x=2;x<=i;x++){
			nextNumber = a + b;
			System.out.print(nextNumber + " ");
			a=b;
			b=nextNumber;
		}
	}
}
